bagage
======

**This is a technical preview, use it a your own risk, expect bugs and missing features.**

Bagage will be a service to access your documents everywhere.

Currently, it is only a WebDAV to S3 proxy.
Later, it may propose a web interface and support synchronization with the Nextcloud client.


how it works
------------

bagage exposes a webdav endpoint behind an HTTP basic auth.
Credentials provided through basic auth are used to authenticate on the LDAP server, then 2 keys `garage_s3_access_key` and `garage_s3_secret_key` are fetched from the user's LDAP profile to authenticate against the S3 server.

For now, these entries must be manually/externally added to the LDAP server prior using the service.

configuration
-------------

bagage is configured through environment variables.
You can get all the keys and their default values in the [config.go](https://git.deuxfleurs.fr/Deuxfleurs/bagage/src/branch/main/config.go#L9) file.


running it
----------

You need a S3 endpoint and a LDAP server running.
Once you correctly set your environment variables, you can simply run it:

```
go run .
```

docker
-------

A simple dockerfile is proposed to build a binary if you want.

