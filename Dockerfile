FROM golang:1.17.0-alpine3.14 as builder
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN apk update && apk add --no-cache ca-certificates && update-ca-certificates
WORKDIR /opt
COPY *.go go.mod go.sum /opt/
RUN go build .

FROM scratch 
COPY --from=builder /opt/bagage /
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
USER 1000:1000
ENTRYPOINT ["/bagage"]
