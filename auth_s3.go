package main

import (
	"net/http"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

/* Check credentials against Minio */
type S3Auth struct {
	WithConfig    *Config
	OnMinioClient MinioClientHandler
	OnFailure     ErrorHandler
}

func (s S3Auth) WithCreds(access_key, secret_key string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mc, err := minio.New(s.WithConfig.Endpoint, &minio.Options{
			Creds:  credentials.NewStaticV4(access_key, secret_key, ""),
			Secure: s.WithConfig.UseSSL,
		})
		if err != nil {
			s.OnFailure.WithError(err).ServeHTTP(w, r)
			return
		}
		s.OnMinioClient.WithMC(mc).ServeHTTP(w, r)
	})
}
