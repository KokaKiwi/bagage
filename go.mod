module git.deuxfleurs.fr/Deuxfleurs/bagage

go 1.16

require (
	github.com/go-ldap/ldap/v3 v3.4.1
	github.com/minio/minio-go/v7 v7.0.12
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d
)
