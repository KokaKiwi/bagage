package main

import (
	"errors"
	"net/http"
)

/*
 * We extract the credentials from the Basic Auth headers
 * (We may think to other ways to pass credentials such as a JWT)
 */
type BasicAuthExtract struct {
	OnNotFound ErrorHandler
	OnCreds    CredsHandler
}

func (b BasicAuthExtract) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	username, password, ok := r.BasicAuth()
	if !ok {
		b.OnNotFound.WithError(errors.New("LDAP. Missing Authentication Header")).ServeHTTP(w, r)
		return
	}
	if username == "" || password == "" {
		b.OnNotFound.WithError(errors.New("LDAP. Username or password cannot be empty")).ServeHTTP(w, r)
		return
	}
	b.OnCreds.WithCreds(username, password).ServeHTTP(w, r)
}
