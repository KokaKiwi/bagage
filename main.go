package main

import (
	"log"
	"net/http"
)

func main() {
	log.Println("=== Starting Bagage ===")
	config := (&Config{}).LoadWithDefault().LoadWithEnv()

	log.Println(config)

	// Assemble components to handle WebDAV requests
	http.Handle(config.DavPath+"/",
		BasicAuthExtract{
			OnNotFound: NotAuthorized{},
			OnCreds: LdapPreAuth{
				WithConfig:      config,
				OnWrongPassword: NotAuthorized{},
				OnFailure:       InternalError{},
				OnCreds: S3Auth{
					WithConfig: config,
					OnFailure:  InternalError{},
					OnMinioClient: WebDav{
						WithConfig: config,
					},
				},
			},
		})

	if err := http.ListenAndServe(config.HttpListen, nil); err != nil {
		log.Fatalf("Error with WebDAV server: %v", err)
	}
}
