package main

import (
	"path"
	"strings"

	"github.com/minio/minio-go/v7"
)

type S3Class int

const (
	ROOT S3Class = 1 << iota
	BUCKET
	COMMON_PREFIX
	OBJECT
	OPAQUE_KEY

	KEY = COMMON_PREFIX | OBJECT | OPAQUE_KEY
)

type S3Path struct {
	path   string
	class  S3Class
	bucket string
	key    string
}

func NewS3Path(path string) S3Path {
	exploded_path := strings.SplitN(path, "/", 3)

	// If there is no bucket name (eg. "/")
	if len(exploded_path) < 2 || exploded_path[1] == "" {
		return S3Path{path, ROOT, "", ""}
	}

	// If there is no key
	if len(exploded_path) < 3 || exploded_path[2] == "" {
		return S3Path{path, BUCKET, exploded_path[1], ""}
	}

	return S3Path{path, OPAQUE_KEY, exploded_path[1], exploded_path[2]}
}

func NewTrustedS3Path(bucket string, obj minio.ObjectInfo) S3Path {
	cl := OBJECT
	if obj.Key[len(obj.Key)-1:] == "/" {
		cl = COMMON_PREFIX
	}

	return S3Path{
		path:   path.Join("/", bucket, obj.Key),
		bucket: bucket,
		key:    obj.Key,
		class:  cl,
	}
}
